###### **This document is a step by step guideline to set up Selenium WebDriver project on Windows 10. It is created by Tomasz Rożek.**

###### **Required tools:** 
1. Java JDK
2. Maven
3. Git
4. IDE software (in this case IntelliJ Idea)

1. To download installation kit Java jdk (recommended jdk8) it is required to visit oracle.com webpage and follow the instructions (https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html), then double-click on downloaded file and go through installation process. 

1 a. It is necessary to set up environmental variables. To do so, go to the properties of the computer, click on Advanced  system settings, pick Advanced tab and press 'Environmental Variables' button. Then paste in the section of 'User variables' the JAVA_HOME path (<path_to_jdk_installation_folder>\jre). In the section of 'System variables' in the record 'Path' add new line and paste Java_jdk path (<path_to_jdk_installation_folder>\bin) 

1 b. To check if the installation is complete open the Windows Command line and type 'java -version' and press 'enter'. The command line should print following output:

java version "1.8.0_251"
Java(TM) SE Runtime Environment (build 1.8.0_251-b08)
Java HotSpot(TM) 64-Bit Server VM (build 25.251-b08, mixed mode)

2. To download Apache Maven go to Apache Maven project and pick the binary zip archive (https://maven.apache.org/download.cgi). When the download is completed, just unzip the archive to chosen directory. 

2 a. To set up maven environmental variable add a new line in the section of 'System variables' and paste maven directory (<path_to_maven_directory>\bin)

2 b. To check if the installation is complete open the Windows Command line and type 'mvn -version' and press 'enter'. The command line should print following output: 

Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: <path_to_maven_directory>\bin\..
Java version: 1.8.0_241, vendor: Oracle Corporation, runtime: <path_to_jdk_installation_folder>\jre
Default locale: pl_PL, platform encoding: Cp1250
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"

3. To download version control system git visit the webpage of the software (https://git-scm.com/downloads), then double-click on downloaded file and go through installation process.

3 a. Check the environmental variable, it should be added automatically to the list. Open the Windows Command line and type 'git --version' and press enter. The command line should print similar output: 

git version 2.25.1.windows.1

4. To download IDE software go to webpage of one of the produces, for example (https://www.jetbrains.com/idea/download/#section=windows or https://www.eclipse.org/downloads/ ). After downloading double-click on installation file and go through it. 


###### **Steps to set up the project**

1. The easiest way to create a project is to use archetypes created by Apache Software Foundation. Go to apache maven archetype page (https://maven.apache.org/archetypes/index.html) and click on maven-archetype-quickstart. It is basic java project.

2. Create project directory and using the Windows Command line go to the directory (command: cd <path_to_directory>). 

3. Copy the line from the maven-archetype-quickstart webpage ('mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4'), paste it to the Command line and press enter. Fill up required values for property (groupId, artefactId, version, package), then type 'y' as a confirmation. 
The command line should print following output: 

[INFO] Project created from Archetype in dir: <path_to_the_project_directory>
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:28 min
[INFO] Finished at: 2020-06-27T11:21:00+02:00
[INFO] ------------------------------------------------------------------------

###### **Steps to set up project repository and connect it with local repository**

1. Using Windows Command line go to the project directory (the folder where pom.xml file is present, command 'cd <path_to_directory>) and type 'git init'. The command line should print following output: 

Initialized empty Git repository in <path_to_the_project_directory>/.git/

2. Specify git configuration settings. Type 'git config --global user.name "<user_name>"' and 'git config --global user.email <user_email_address>'

3. Create repository on one of the available webpages for example bitbucket or github. 

4. Connect remote repository with local repository by typing 'git remote add origin https://<address_of_the_remote_repository>.git'. 

4 a. Check if the git repositories are connected by typing 'git fetch'. The command line should print similar output: 

remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), 538 bytes | 134.00 KiB/s, done.
From <address_of_the_remote_repository>
 * [new branch]      master     -> origin/master
 
5. Then merge local repository with remote repository by typing 'git pull origin master --allow-unrelated-histories'

6. Create .gitignore file and paste following content: 
.idea/ 
*.iml
/target/
 
###### **Steps to configure Selenium WebDriver project**
 
 1. Run the IDE software (In this case Idea IntelliJ)
 
 2. Open up the project: File>New>Project from Existing sources. To open up the project find the pom.xml file, select it and click 'OK' button. 
 
 3. Change two properties in the file to  <maven.compiler.source>1.8</maven.compiler.source>, <maven.compiler.target>1.8</maven.compiler.target>
 
 4. Add required and recommended dependencies (all of them are available to download from https://mvnrepository.com/): 
 
 <!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java -->
<dependency>
    <groupId>org.seleniumhq.selenium</groupId>
    <artifactId>selenium-java</artifactId>
    <version>3.141.59</version>
</dependency>

<!-- https://mvnrepository.com/artifact/io.github.bonigarcia/webdrivermanager -->
<dependency>
    <groupId>io.github.bonigarcia</groupId>
    <artifactId>webdrivermanager</artifactId>
    <version>4.0.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.12</version>
    <scope>provided</scope>
</dependency>

<!-- https://mvnrepository.com/artifact/org.testng/testng -->
<dependency>
    <groupId>org.testng</groupId>
    <artifactId>testng</artifactId>
    <version>7.1.0</version>
    <scope>test</scope>
</dependency>

###### **Steps to start writing tests**

1. Make a new remote branch in the repository

2. Run Windows command line and go to the directory of local repository

3. Type 'git fetch && git checkout <name_of_the_branch>' and press Enter

4. Create Tests suites directory

5. Create Test cases index file

6. Develop the test classes

###### **Steps to push finished work to repository**

1. Open Windows command line and go to local repository directory 

2. Type in command 'git status'

3. The command line should print similar output: 
On branch mainPageTests
   Your branch is up to date with 'origin/mainPageTests'.
   
   Changes to be committed:
     (use "git restore --staged <file>..." to unstage)
           deleted:    src/main/java/rozek/tomek/App.java
           new file:   src/test/java/mainPageTests/MainPageTest.java
           deleted:    src/test/java/rozek/tomek/AppTest.java
   
   Changes not staged for commit:
     (use "git add <file>..." to update what will be committed)
     (use "git restore <file>..." to discard changes in working directory)
           modified:   README.md
           modified:   pom.xml
           modified:   src/test/java/mainPageTests/MainPageTest.java
   
   Untracked files:
     (use "git add <file>..." to include in what will be committed)
           TestSuites/

4. To add files to commit type 'git add <files_to_commit>'. If there is more than one file type in the names with space between the names.
(In the case above 'git add src/test/java/mainPageTests/MainPageTest.java README.md pom.xml TestSuites/ ')

5. Commit added file using comment 'git commit -m "<message_describing_commit>"'.

6. Push committed files to repository using comment 'git push origin mainPageTests'

7. Log in to remote repository and create pull requests so as to mainPageTests can be pushed to the master

###### **Steps to run tests using Jenkins continuous integrations software** 
(for the purpose of this guideline Jenkins have been deployed on Tomcat server at VPS 
http://www.trenterprise.cloud:8080/jenkins/ ).

1. Download Jenkins Generic Java package (.war) from https://www.jenkins.io/download/

2. Run Jenkins at localhost and go through configuration process.

3. Add new job to Jenkins 

More to read Selenium i testowanie aplikacji Receptury by Unmesh Gundecha, pp. 281- 285.

###### **References**
1. Selenium i testowanie aplikacji Receptury by Unmesh Gundecha. Polish edition copyright © 2017 by Helion SA
