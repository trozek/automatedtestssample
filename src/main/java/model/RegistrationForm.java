/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

package model;

import lombok.*;

/*
* The RegistrationForm class serves as a model of the registration form the web page.
* The class uses lombok annotation (listed below).
* The builder annotation is used to simplify the creation of the object.
*/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistrationForm {

    private String firstName;
    private String lastName;
    private String password;
    private int dayOfBirth;
    private String monthOfBirth;
    private int yearOfBirth;
    private String company;
    private String address;
    private String city;
    private String state;
    private int postalCode;
    private String country;
    private int phoneNumber;
    private String addressAlias;

}
