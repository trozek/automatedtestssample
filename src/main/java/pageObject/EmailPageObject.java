/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

package pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/*
*
* This class consists of Web elements used in RegistrationEmailTest.
* Annotation @FindBy requires parameter to identify WebElement.
* The elements are visible to web driver after the initiation by PageFactory.
*
* */
public class EmailPageObject {

    @FindBy(id = "email_create")
    public static WebElement EMAIL_INPUT_FIELD;

    @FindBy(id = "SubmitCreate")
    public static WebElement CREATE_ACCOUNT_BUTTON;

    @FindBy(className = "alert-danger")
    public static WebElement ALERT_DANGER;

}
