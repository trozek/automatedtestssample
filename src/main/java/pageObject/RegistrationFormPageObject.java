/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

package pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/*
* This class consists of WebElements list used by PageFactory in the RegistrationFormTest class.
* @FindBy annotation gives the definition of web element given on the web page.
*/

public class RegistrationFormPageObject {

    @FindBy(id = "submitAccount")
    public static WebElement REGISTER_BUTTON;

    @FindBy(className = "alert-danger")
    public static WebElement ALERT;

    @FindBy(id = "id_gender1")
    public static WebElement MR_RADIO_BUTTON;

    @FindBy(id = "id_gender2")
    public static WebElement MRS_RADIO_BUTTON;

    @FindBy(id = "customer_firstname")
    public static WebElement FIRST_NAME;

    @FindBy(id = "customer_lastname")
    public static WebElement LAST_NAME;

    @FindBy(id = "passwd")
    public static WebElement PASSWORD;

    @FindBy(id = "days")
    public static WebElement DAY_OF_BIRTH;

    @FindBy(id = "months")
    public static WebElement MONTH_OF_BIRTH;

    @FindBy(id = "years")
    public static WebElement YEAR_OF_BIRTH;

    @FindBy(id = "company")
    public static WebElement COMPANY;

    @FindBy(id = "address1")
    public static WebElement ADDRESS;

    @FindBy(id = "city")
    public static WebElement CITY;

    @FindBy(id = "id_state")
    public static WebElement STATE;

    @FindBy(id = "postcode")
    public static WebElement POSTAL_CODE;

    @FindBy(id = "id_country")
    public static WebElement COUNTRY;

    @FindBy(id = "phone_mobile")
    public static WebElement MOBILE_PHONE;

    @FindBy(id = "alias")
    public static WebElement ADDRESS_ALIAS;

}
