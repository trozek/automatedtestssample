/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

package dataProvider;


import model.RegistrationForm;
import org.testng.annotations.DataProvider;

/*
* This class consists of the object RegistrationForm.
* @DataProvider annotation with required 'name' parameter signs the object as data supplier
* to the tests covering the registration form included in RegistrationFormTest class.
* The object itself is based on model defined in RegistrationForm class in package model and assembled by builder pattern.
*/

public class RegistrationFormDataProvider {

    @DataProvider(name = "registrationFormCorrectData")
    public static Object[] registrationForm(){
        RegistrationForm registrationForm = RegistrationForm.builder()
                .firstName("FirstName")
                .lastName("LastName")
                .password("Password")
                .dayOfBirth(1)
                .monthOfBirth("January")
                .yearOfBirth(2000)
                .company("CompanyName")
                .address("FirstLineOfTheAddress")
                .city("NameOfTheCity")
                .state("Alabama")
                .postalCode(23445)
                .country("United States")
                .phoneNumber(123456789)
                .addressAlias("Alias")
                .build();
        return new Object[] {registrationForm};
    }

}
