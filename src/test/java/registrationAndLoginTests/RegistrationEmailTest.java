/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

package registrationAndLoginTests;


import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObject.EmailPageObject;

import static org.testng.Assert.*;
import static pageObject.EmailPageObject.*;

/*
 * This class tests registration email field. Test cases are listed in "TestSuites/002. CreateAnAccountEmailAddressCheck.xlsx"
 * It uses Lombok @Log annotation to put down the logs of used methods.
 * The webdriver is delivered by Chrome Driver manager to avoid downloading .exe file.
 * All tests are managed by testng annotation @Test. In the brackets is given tests ordering parameter 'priority'.
 * To run tests click on the green triangle on the left side of the class name or go to 'Run/Debug configuration' and pick up TestNG configuration select the class.
 * In the init() it is initiated the PageFactory method, which makes the elements listed in EmailPageObject visible to webdriver
 * During the tests the WebDriverWait object is created (WebDriver Explicit Wait). It holds test examination for 10 seconds before throwing the exception or given condition is true.
 *
 */

@Log
public class RegistrationEmailTest {

    static WebDriver driver;
    private static String WEBSITE = "http://automationpractice.com/index.php?controller=authentication&back=my-account";

    // Method below sets up the driver and opens the browser window
    // Added parameter "--headless" to ChromeDriver object to enable jenkins test execution
    @BeforeClass
    public static void setUpDriver() {
        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--headless");
        driver = new ChromeDriver(option);
        log.info("The Chrome Driver is set up and the window is open");
    }

    // Method below maximizes browser window, opens the web page and initialize Page Factory class.
    // initElements(driver, RegistrationAndLoginPageObject.class) method requires two parameters:
    // first is driver reference and second is class reflection (pageClassToProxy)
    @BeforeMethod
    public static void init() {
        driver.manage().window().maximize();
        driver.get(WEBSITE);
        PageFactory.initElements(driver, EmailPageObject.class);
        log.info("The web site is open");
    }

    @Test(priority = 1)
    public static void empty_field_is_invalid() {
        log.info("Test checking whether the empty field is invalid has started");
        CREATE_ACCOUNT_BUTTON.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(ALERT_DANGER));
        assertTrue(driver.findElement(By.className("alert-danger")).isDisplayed());
    }

    @Test(priority = 1)
    public static void incorrect_email_is_invalid() {
        log.info("Test checking whether the incorrect email address is invalid has started");
        EMAIL_INPUT_FIELD.sendKeys("asdfasdfasdf");
        CREATE_ACCOUNT_BUTTON.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(ALERT_DANGER));
        assertTrue(driver.findElement(By.className("alert-danger")).isDisplayed());
    }

    @Test(priority = 2)
    public static void correct_email_is_valid() {
        log.info("Test checking whether the correct email address is valid");
        EMAIL_INPUT_FIELD.sendKeys("asdfasdf@fasdf.pl");
        CREATE_ACCOUNT_BUTTON.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlContains("account-creation"));
        assertTrue(driver.getCurrentUrl().contains("account-creation"));
    }

    // Method below kills web driver
    @AfterClass
    public static void tearDown() {
        driver.close();
        log.info("Web driver has been killed");
    }
}
