/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

package registrationAndLoginTests;

import dataProvider.RegistrationFormDataProvider;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import lombok.extern.java.Log;
import model.RegistrationForm;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObject.EmailPageObject;
import pageObject.RegistrationFormPageObject;

import java.util.Random;

import static pageObject.EmailPageObject.*;
import static pageObject.RegistrationFormPageObject.*;
import static org.testng.Assert.*;

/* This class tests registration email field. Test cases are listed in "TestSuites/003. RegistrationFormTests.xlsx"
* It uses Lombok @Log annotation to put down the logs of used methods.
* The webdriver is delivered by Chrome Driver manager to avoid downloading .exe file.
* All tests are managed by testng annotation @Test. In the brackets is given tests ordering parameter 'priority'.
* To run tests click on the green triangle on the left side of the class name or go to 'Run/Debug configuration' and pick up TestNG configuration select the class.
* In the init() it is initiated the PageFactory method, which makes the elements listed in RegistrationFormPageObject visible to webdriver
* During the tests the WebDriverWait object is created (WebDriver Explicit Wait). It holds test examination for 10 seconds before throwing the exception or given condition is true.
*/

@Log
public class RegistrationFormTest {

    static WebDriver driver;
    private static String WEBSITE = "http://automationpractice.com/index.php?controller=authentication&back=my-account";

    // Method below sets up the driver and opens the browser window
    // Added parameter "--headless" to ChromeDriver object to enable jenkins test execution
    @BeforeClass
    public static void setUpDriver() {
        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--headless");
        driver = new ChromeDriver(option);
        log.info("The Chrome Driver is set up and the window is open");
    }

    // Method below maximizes browser window, opens the web page and initialize Page Factory class.
    // initElements(driver, EmailPageObject.class) method requires two parameters:
    // first is driver reference and second is class reflection (pageClassToProxy)
    @BeforeMethod
    public static void init() {
        driver.manage().window().maximize();
        driver.get(WEBSITE);
        PageFactory.initElements(driver, EmailPageObject.class);
        Random random = new Random();
        EMAIL_INPUT_FIELD.sendKeys(random.nextInt(100000000)+ "@fasdf.pl");
        CREATE_ACCOUNT_BUTTON.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlContains("account-creation"));
        PageFactory.initElements(driver, RegistrationFormPageObject.class);
        log.info("The web site is open");
    }

    @Test(priority = 1)
    public static void empty_form_is_validated(){
        log.info("Test checking whether the incorrect email address is invalid has started");
        REGISTER_BUTTON.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className("alert-danger"))));
        assertTrue(ALERT.isDisplayed());
    }

    // In the test method below @Test annotation includes 'priority' and dataProvider description - name of it and the class where it could be found.
    // The method itself as a parameter uses RegistrationForm object to get access to values given to it in the DataProvider.
    @Test(priority = 1, dataProvider = "registrationFormCorrectData", dataProviderClass = RegistrationFormDataProvider.class)
    public static void registration_is_successful(RegistrationForm registrationForm){
        log.info("Test checking whether the registration is successful has started");
        MRS_RADIO_BUTTON.click();
        FIRST_NAME.sendKeys(registrationForm.getFirstName());
        LAST_NAME.sendKeys(registrationForm.getLastName());
        PASSWORD.sendKeys(registrationForm.getPassword());
        DAY_OF_BIRTH.sendKeys(String.valueOf(registrationForm.getDayOfBirth()));
        MONTH_OF_BIRTH.sendKeys(registrationForm.getMonthOfBirth());
        YEAR_OF_BIRTH.sendKeys(String.valueOf(registrationForm.getYearOfBirth()));
        COMPANY.sendKeys(registrationForm.getCompany());
        ADDRESS.sendKeys(registrationForm.getAddress());
        CITY.sendKeys(registrationForm.getCity());
        STATE.sendKeys(registrationForm.getState());
        POSTAL_CODE.sendKeys(String.valueOf(registrationForm.getPostalCode()));
        COUNTRY.sendKeys(registrationForm.getCountry());
        MOBILE_PHONE.sendKeys(String.valueOf(registrationForm.getPhoneNumber()));
        ADDRESS_ALIAS.sendKeys(registrationForm.getAddressAlias());
        REGISTER_BUTTON.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlContains("controller=my-account"));
        assertTrue(driver.getCurrentUrl().equals("http://automationpractice.com/index.php?controller=my-account"));
    }

    //Method below kills web driver
    @AfterClass
    public static void tearDown() {
        driver.close();
        log.info("Web driver has been killed");
    }
}
