package mainPageTests;

/*
 *
 *
 *   @ Tomasz Rożek 2020
 *
 *
 */

import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/*
 * This class tests index page. Test cases are listed in "TestSuites/001. MainPageTests.xlsx"
 * It uses Lombok @Log annotation to put down the logs of used methods.
 * The webdriver is delivered by Chrome Driver manager to avoid downloading .exe file.
 * All tests are managed by testng annotation @Test. In the brackets is given tests ordering parameter 'priority'.
 * To run tests click on the green triangle on the left side of the class name or go to 'Run/Debug configuration' and pick up TestNG configuration select the class.
 *
 * */
@Log
public class MainPageTest {

    static WebDriver driver;
    private static String WEBSITE = "http://automationpractice.com/index.php";

    //Method below sets up the driver and opens the browser window, maximizes browser window and opens the web page
    // Added parameter "--headless" to ChromeDriver object to enable jenkins test execution
    @BeforeClass
    public static void setUpDriver() {
        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--headless");
        driver = new ChromeDriver(option);
        log.info("The Chrome Driver is set up and the window is open");
    }

    @BeforeMethod
    public static void init() {
        driver.manage().window().maximize();
        driver.get(WEBSITE);
        log.info("The web site is open");
    }

    @Test(priority = 1)
    public static void web_page_is_loaded() {
        log.info("Test checking whether the web page is loaded has started");
        assertTrue(driver.findElement(By.className("logo")).isDisplayed());
    }

    @Test(priority = 2)
    public static void contact_us_button_is_displayed() {
        log.info("Test checking whether 'contact us' button is displayed has started");
        assertTrue(driver.findElement(By.id("contact-link")).isDisplayed());
    }

    @Test(priority = 2)
    public static void sign_in_button_is_displayed() {
        log.info("Test checking whether 'sign in' button is displayed has started");
        assertTrue(driver.findElement(By.className("login")).isDisplayed());
    }

    @Test(priority = 2)
    public static void button_has_sign_in_label() {
        log.info("Test checking whether 'Sign in' button has proper name has started");
        String label = driver.findElement(By.className("login")).getText();
        assertTrue(label.contains("Sign in"));
    }

    @Test(priority = 2)
    public static void search_bar_is_displayed() {
        log.info("Test checking whether search bar is displayed has started");
        assertTrue(driver.findElement(By.id("search_query_top")).isDisplayed());
    }

    @Test(priority = 3)
    public static void search_bar_is_working() {
        log.info("Test checking whether search bar is working has started");
        WebElement searchBar = driver.findElement(By.id("search_query_top"));
        searchBar.sendKeys("WomanDressTest");
        searchBar.sendKeys(Keys.ENTER);
        assertTrue(driver.getCurrentUrl().contains("search_query=WomanDressTest"));
    }

    @Test(priority = 2)
    public static void cart_is_displayed() {
        log.info("Test checking whether cart is displayed has started");
        assertTrue(driver.findElement(By.className("shopping_cart")).isDisplayed());
    }

    @Test(priority = 2)
    public static void cart_is_empty() {
        log.info("Test checking whether cart is empty has started");
        assertFalse(driver.findElement(By.className("ajax_cart_quantity")).getText().contains("1"));
    }

    //Method below kills web driver
    @AfterClass
    public static void tearDown() {
        driver.close();
        log.info("Web driver has been killed");
    }
}
